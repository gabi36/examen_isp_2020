import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

public class Ex2 extends JFrame {

    JButton button;
    JTextArea area;

    Ex2(){
        setTitle("Add random number");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300,250);
        init();
        setVisible(true);
    }

    public void init(){
        int width=50;
        int heigth=20;
        this.setLayout(null);

        button=new JButton();
        button.setBounds(100, 50, 70, heigth);
        button.setText("Click");

        area = new JTextArea();
        area.setBounds(100, 100, 70, 50);

        add(button); add(area);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                Random r = new Random();
                int nr = r.nextInt(10);
                area.append(nr+"\n");
            }
        });
    }

    public static void main(String[] args) {
        new Ex2();
    }
}
