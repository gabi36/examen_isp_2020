public class Ex1 {
    class U{
        public void p(){}
    }

    class J{

    }

    class I extends U{
        K k;
        private long t;

        public void f(){}
        public void i(J j){}
    }

    class N{
        I i;
    }

    class L{
        public void metA(){}
    }

    class K{
        L l;
        public void K(){
            l = new L();
        }
    }
    class S{
        public void metB(){}
        K k;
    }
}
